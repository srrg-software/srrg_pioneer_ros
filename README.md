# srrg2_pioneer

This package contains a minimalistic driver for the pioneer robot,
based on the old **CARMEN** (carnegie mellon navigation toolkit) library.
The code is released according to the most restrictive license (GPLV2),
since I use CARMEN stuff which is based on that license.

## Setting up
Just copy the node in your ros_package_path and compile

Parameters:
* `_serial_device`: (/dev/ttyUSB0) the serial port
* `_odom_topic`: (/odom) the odometry topic 
* `_command_vel_topic`: (/cmd_vel) the cmd_vel topic
* `_joint_state_topic`: (/joint_state) joint state topic for use with calibration tools
* `_odom_frame_id`: (/odom) frame id for the odometry message
* `_base_link_frame_id`: (/base_link) frame id for the odometry message
* `_publish_tf`: set to true publishes the tf between the odom and base_link

That's all.

## Authors

* **Giorgio Grisetti**


## License

GPLV2
